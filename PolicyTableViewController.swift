//
//  PolicyTableViewController.swift
//  HackIllinois
//
//  Created by Sanjeet Suhag on 2/20/16.
//  Copyright © 2016 Sanjeet Suhag. All rights reserved.
//

import UIKit

class PolicyTableViewController: UITableViewController {

    
    // MARK: - Properties
    
    var policies: [Policy]!
    var selection: Policy!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        policies = [
            Policy(category: Category.ArtsCulture),
            Policy(category: Category.Crime),
            Policy(category: Category.Education),
            Policy(category: Category.Environment),
            Policy(category: Category.Families),
            Policy(category: Category.ForeignPolicy),
            Policy(category: Category.Government),
            Policy(category: Category.Healthcare),
            Policy(category: Category.Immigration),
            Policy(category: Category.JobsEconomy),
            Policy(category: Category.Other),
            Policy(category: Category.ScienceTech),
            Policy(category: Category.Taxes)
        ]
        
        selection = nil
        
        self.title = "POLICIES"
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        for tabBarItem in (self.tabBarController?.tabBar.items)! {
            tabBarItem.title = "";
            tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return policies.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PolicyCell", forIndexPath: indexPath)

        // Configure the cell...
        cell.textLabel?.text = self.policies[indexPath.row].category.rawValue
        cell.textLabel?.font = UIFont(name: "Lora-Regular", size: 18)
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        selection = policies[indexPath.row]
        performSegueWithIdentifier("ShowPolicySegue", sender: self)
        
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 64
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "ShowPolicySegue") {
            
            let pageViewController = segue.destinationViewController as! PolicyPageViewController
            pageViewController.policy = selection
            
        }
    }

}
