//
//  APIHandler.swift
//  HackIllinois
//
//  Created by Sanjeet Suhag on 2/19/16.
//  Copyright © 2016 Sanjeet Suhag. All rights reserved.
//

// MARK: URLs

let POLITIPLATFORM_URL = "https://politiplatform.com"
let TWITTER_URL = "https://twitter.com/"

let CRUZ_URL = "/cruz"
let TRUMP_URL = "/trump"
let RUBIO_URL = "/rubio"
let CARSON_URL = "/carson"
let CLINTON_URL = "/clinton"
let SANDERS_URL = "/sanders"

let ARTS_CULTURE_URL = "/arts+culture"
let CRIME_URL = "/crime"
let EDUCATION_URL = "/education"
let ENVIRONMENT_URL = "/environment"
let FAMILIES_URL = "/families"
let FOREIGN_POLICY_URL = "/foreign+policy"
let GOVERNMENT_URL = "/government"
let HEALTHCARE_URL = "/healthcare"
let IMMIGRATION_URL = "/immigration"
let JOBS_ECONOMY_URL = "/jobs+economy"
let OTHER_URL = "/other"
let SCIENCE_TECH_URL = "/science+tech"
let TAXES_URL = "/taxes"

let regex: NSRegularExpression = try! NSRegularExpression(pattern: "\n*\n\\d+", options: NSRegularExpressionOptions.CaseInsensitive)

import Foundation
import Kanna
import Async

enum Category: String {
    case ArtsCulture = "Arts & Culture"
    case Crime = "Crime"
    case Education = "Education"
    case Environment = "Environment"
    case Families = "Families"
    case ForeignPolicy = "Foreign Policy"
    case Government = "Government"
    case Healthcare = "Healthcare"
    case Immigration = "Immigration"
    case JobsEconomy = "Jobs & Economy"
    case Other = "Other"
    case ScienceTech = "Science & Tech"
    case Taxes = "Taxes"
}

    
//// MARK: APIHandler Class
//
//private let _sharedAPIHandler = APIHandler()
//
//public class APIHandler: NSObject {
//    
//    class var sharedAPIHandler: APIHandler {
//        return _sharedAPIHandler
//    }
//    
//    var candidates = [Candidate]()
//    
//    override init() {
//        
//        let TedCruz = Candidate(name: "Ted Cruz", url: "https://www.tedcruz.org/", twitter: "tedcruz", age: 45, image: "https://politiplatform.com/img/politicians/ted_cruz/avatar.jpg", position: "Senator from Texas", party: "Republican Party", policyUrl: CRUZ_URL, bgImage: "https://politiplatform.com/img/politicians/ted_cruz/bg.jpg")
//        
//        let DonaldTrump = Candidate(name: "Donald Trump", url: "https://www.donaldjtrump.com/", twitter: "realDonaldTrump", age: 69, image: "https://politiplatform.com/img/politicians/donald_trump/avatar.jpg", position: "Real estate developer", party: "Republican Party", policyUrl: TRUMP_URL, bgImage: "https://politiplatform.com/img/politicians/donald_trump/bg.jpg")
//        
//        let MarcoRubio = Candidate(name: "Marco Rubio", url: "https://marcorubio.com/", twitter: "marcorubio", age: 44, image: "https://politiplatform.com/img/politicians/marco_rubio/avatar.jpg", position: "Senator from Florida", party: "Republican Party", policyUrl: RUBIO_URL, bgImage: "https://politiplatform.com/img/politicians/marco_rubio/bg.jpg")
//        
//        let BenCarson = Candidate(name: "Ben Carson", url: "https://www.bencarson.com/", twitter: "realbencarson", age: 64, image: "https://politiplatform.com/img/politicians/ben_carson/avatar.jpg", position: "Retired pediatric neurosurgeon", party: "Republican Party", policyUrl: CARSON_URL, bgImage: "https://politiplatform.com/img/politicians/ben_carson/bg.jpg")
//        
//        let HillaryClinton = Candidate(name: "Hillary Clinton", url: "https://www.hillaryclinton.com/", twitter: "votehillary2016", age: 68, image: "https://politiplatform.com/img/politicians/hillary_clinton/avatar.jpg", position: "Former Senator from New York", party: "Democratic Party", policyUrl: CLINTON_URL, bgImage: "https://politiplatform.com/img/politicians/hillary_clinton/bg.jpg")
//        
//        let BernieSanders = Candidate(name: "Bernie Sanders", url: "https://berniesanders.com/", twitter: "sensanders", age: 74, image: "https://politiplatform.com/img/politicians/bernie_sanders/avatar.jpg", position: "Senator from Vermont", party: "Democratic Party", policyUrl: SANDERS_URL, bgImage: "https://politiplatform.com/img/politicians/bernie_sanders/bg.jpg")
//        
//        candidates = [HillaryClinton, BernieSanders, TedCruz, DonaldTrump, MarcoRubio, BenCarson]
//        var policies = [Policy]()
//        
//        for candidate in candidates {
//
//            policies.appendContentsOf(candidate.policies)
//        }
//    }
//}
//

class APIClient {
    
    static let sharedAPIClient = APIClient()
    
    // MARK: - Properties
    
    var candidates: [Candidate]
    var policies: [Policy]
    
    init() {
        
        let TedCruz = Candidate(name: "Ted Cruz", url: "https://www.tedcruz.org/", twitter: "tedcruz", age: 45, image: "https://politiplatform.com/img/politicians/ted_cruz/avatar.jpg", position: "Senator from Texas", party: "Republican Party", policyUrl: CRUZ_URL, bgImage: "https://politiplatform.com/img/politicians/ted_cruz/bg.jpg")
        
        let DonaldTrump = Candidate(name: "Donald Trump", url: "https://www.donaldjtrump.com/", twitter: "realDonaldTrump", age: 69, image: "https://politiplatform.com/img/politicians/donald_trump/avatar.jpg", position: "Real estate developer", party: "Republican Party", policyUrl: TRUMP_URL, bgImage: "https://politiplatform.com/img/politicians/donald_trump/bg.jpg")
        
        let MarcoRubio = Candidate(name: "Marco Rubio", url: "https://marcorubio.com/", twitter: "marcorubio", age: 44, image: "https://politiplatform.com/img/politicians/marco_rubio/avatar.jpg", position: "Senator from Florida", party: "Republican Party", policyUrl: RUBIO_URL, bgImage: "https://politiplatform.com/img/politicians/marco_rubio/bg.jpg")
        
        let BenCarson = Candidate(name: "Ben Carson", url: "https://www.bencarson.com/", twitter: "realbencarson", age: 64, image: "https://politiplatform.com/img/politicians/ben_carson/avatar.jpg", position: "Retired pediatric neurosurgeon", party: "Republican Party", policyUrl: CARSON_URL, bgImage: "https://politiplatform.com/img/politicians/ben_carson/bg.jpg")
        
        let HillaryClinton = Candidate(name: "Hillary Clinton", url: "https://www.hillaryclinton.com/", twitter: "votehillary2016", age: 68, image: "https://politiplatform.com/img/politicians/hillary_clinton/avatar.jpg", position: "Former Senator from New York", party: "Democratic Party", policyUrl: CLINTON_URL, bgImage: "https://politiplatform.com/img/politicians/hillary_clinton/bg.jpg")
        
        let BernieSanders = Candidate(name: "Bernie Sanders", url: "https://berniesanders.com/", twitter: "sensanders", age: 74, image: "https://politiplatform.com/img/politicians/bernie_sanders/avatar.jpg", position: "Senator from Vermont", party: "Democratic Party", policyUrl: SANDERS_URL, bgImage: "https://politiplatform.com/img/politicians/bernie_sanders/bg.jpg")
        
        self.candidates = [BernieSanders, HillaryClinton, TedCruz, DonaldTrump, MarcoRubio, BenCarson]
        self.policies = [Policy]()
        
        for candidate in self.candidates {
            policies.appendContentsOf(candidate.policies)
        }
        
    }
    
}