//
//  PolicyPageViewController.swift
//  HackIllinois
//
//  Created by Sanjeet Suhag on 2/20/16.
//  Copyright © 2016 Sanjeet Suhag. All rights reserved.
//

import Hue
import UIKit

class PolicyPageViewController: UIPageViewController {

    // MARK: - Properties
    
    var pages = [CandidateViewController]()
    
    var navigationBar = UINavigationBar()
    
    var policy: Policy!
    
    var candidates: [Candidate]!
    
    var gestureRecognizer: UISwipeGestureRecognizer!
    
    var selectedCandidates = [Candidate]()
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Initialise properties.
        
        self.candidates = APIClient.sharedAPIClient.candidates
        
        
        // Setup gestures.
        
        gestureRecognizer = UISwipeGestureRecognizer(target: self, action: "handleSwipe:")
        gestureRecognizer.direction = .Down
        
        // Setup view.
        
        setNavigationBarToView()
        
        // Setup data.
        
        removeUnwantedCandidates()
        
        // Setup pages.
        
        setupPages()
        
        if self.policy.category != Category.ArtsCulture {
            dataSource = self
        }
        
        setViewControllers([pages.first!], direction: .Forward, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - UI Setup
    
    func setNavigationBarToView() {
        
        // Set navigation bar properties.
        
        navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 66)
        navigationBar.barStyle = .Default
        navigationBar.backgroundColor = UIColor.americanRed()
        navigationBar.addGestureRecognizer(gestureRecognizer)
    
        
        // Initialise label.
        
        let policyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 30))
        policyLabel.font = UIFont(name: "Lora-Bold", size: 18)
        policyLabel.textColor = UIColor.whiteColor()
        policyLabel.text = policy.category.rawValue.capitalizedString
        policyLabel.sizeToFit()
        policyLabel.center = navigationBar.center
        navigationBar.addSubview(policyLabel)

        // Add navigation bar to view.
        
        self.view.addSubview(navigationBar)
    }
    
    // MARK: - Data Setup
    
    func removeUnwantedCandidates() {
        
        // Loop through all candidates.
        
        for candidate in candidates {
            
            // Check if the candidate has the current policy.
            
            for candidatePolicy in candidate.policies {
                
                if candidatePolicy.category.rawValue == policy.category.rawValue {
                    
                    if !self.selectedCandidates.contains(candidate) {
                        self.selectedCandidates.append(candidate)
                    }
                    
                }
                
            }
            
        }
    }
    
    // MARK: - Pages Setup
    
    func setupPages() {
        
        // Loop through all valid candidates.
        
        for candidate in selectedCandidates {
            
            // Setup page.
            
            let page = self.storyboard!.instantiateViewControllerWithIdentifier("CandidateViewController") as! CandidateViewController
            page.candidate = candidate
            
            // Add page to pages.
            
            pages.append(page)
            
        }
        
        for (index, page) in pages.enumerate() {
            
            page.pageIndex = index
        
        }
        
        if (pages.count == 1) {
            pages.append(pages.first!)
        }
        
    }
}

// MARK: - UIPageViewControllerDataSource

extension PolicyPageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        var index = (viewController as! CandidateViewController).pageIndex!
        index--
        
        if (index < 0) {
            index = pages.count - 1
        }
        
        return pages[index]
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        var index = (viewController as! CandidateViewController).pageIndex!
        index++
        
        if (index >= pages.count) {
            index = 0
        }
        
        return pages[index]
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    func handleSwipe (gestureRecognizer: UISwipeGestureRecognizer) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}