//
//  Policy.swift
//  HackIllinois
//
//  Created by Sanjeet Suhag on 2/20/16.
//  Copyright © 2016 Sanjeet Suhag. All rights reserved.
//

import UIKit

class Policy: NSObject {
    var category: Category!
    var information: String!
    var candidate: Candidate!
    
    init(category: Category, info: String, candidate: Candidate) {
        
        super.init()
        
        self.category = category
        self.information = info
        self.candidate = candidate
        
    }
    
    init(category: Category) {
        
        super.init()
        
        self.category = category
        
    }
}
