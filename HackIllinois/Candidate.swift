//
//  Candidate.swift
//  HackIllinois
//
//  Created by Sanjeet Suhag on 2/20/16.
//  Copyright © 2016 Sanjeet Suhag. All rights reserved.
//

import Async
import Kanna
import UIKit

class Candidate: NSObject {
    
    var name: String!
    var url: String!
    var twitter: String!
    var age: UInt!
    var image: String!
    var position: String!
    var party: String!
    var imageData: UIImage!
    var policyUrl: String!
    var policies: [Policy]!
    var bgImage: String!
    var bgImageData: UIImage!
    
    init(name: String, url: String, twitter: String, age: UInt, image: String, position: String, party: String, policyUrl: String, bgImage: String) {
        
        super.init()
        
        self.name = name
        self.url = url
        self.twitter = twitter
        self.age = age
        self.image = image
        self.position = position
        self.party = party
        self.policyUrl = policyUrl
        self.policies = [Policy]()
        self.bgImage = bgImage
        
        
        self.downloadImage()
        Async.background() {
            self.downloadPolicies()
        }
        
    }
    
    func downloadImage() {
       
        self.bgImageData = UIImage(data: NSData(contentsOfURL: NSURL(string: self.bgImage)!)!)
        self.imageData = UIImage(data: NSData(contentsOfURL: NSURL(string: self.image)!)!)
    }
    
    func downloadPolicies() {
        let html: NSString = try! NSString(contentsOfURL: NSURL(string: "\(POLITIPLATFORM_URL)\(policyUrl)/all")!, encoding: NSUTF8StringEncoding)
        self.policies = policiesFromHTML(html as String)
    }
    
    func policiesFromHTML(htmlText:String) -> ([Policy]) {
        
        var policies = [Policy]()
        
        if let doc = Kanna.HTML(html: htmlText as String, encoding: NSUTF8StringEncoding) {
            
            var currentCategory: Category = Category.Other
            
            for list in doc.xpath("//tr") {
                if (list.className == "header") {
                    currentCategory = categoryFromTitle(list.text!)
                }
                
                let policyString = regex.stringByReplacingMatchesInString(list.text!, options: .WithoutAnchoringBounds, range: NSMakeRange(0, list.text!.characters.count), withTemplate: "")
                let policy = policyString.stringByReplacingOccurrencesOfString("\n", withString: "")
                if currentCategory.rawValue.containsString(policy) == false {
                    policies.append(Policy(category: currentCategory, info: policy, candidate: self))
                }
            }
        }
        
    
        
        return policies
    }
    
    func categoryFromTitle(text: String) -> Category {
        
        if (text.containsString("Arts")) {
            return Category.ArtsCulture
        } else if (text.containsString("Crime")) {
            return Category.Crime
        } else if (text.containsString("Education")) {
            return Category.Education
        } else if (text.containsString("Environment")) {
            return Category.Environment
        } else if (text.containsString("Families")) {
            return Category.Families
        } else if (text.containsString("Foreign")) {
            return Category.ForeignPolicy
        } else if (text.containsString("Government")) {
            return Category.Government
        } else if (text.containsString("Healthcare")) {
            return Category.Healthcare
        } else if (text.containsString("Immigration")) {
            return Category.Immigration
        } else if (text.containsString("Jobs")) {
            return Category.JobsEconomy
        } else if (text.containsString("Science")) {
            return Category.ScienceTech
        } else if (text.containsString("Taxes")) {
            return Category.Taxes
        } else {
            return Category.Other
        }
    }
}