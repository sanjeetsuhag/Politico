//
//  IssueHeaderView.swift
//  HackIllinois
//
//  Created by Sanjeet Suhag on 2/20/16.
//  Copyright © 2016 Sanjeet Suhag. All rights reserved.
//

import UIKit

class IssueHeaderView: UIView {
    
    // MARK: Properties
    
    var label: UILabel!
    
    // MARK: Lifecycle
    
    convenience init() {
        self.init(frame: CGRectZero)
        setupSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupSubviews()
    }
    
    func setupSubviews() {
        
        backgroundColor = UIColor.whiteColor()
        
        label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Lora-Bold", size: 16)
        label.textColor = UIColor.americanRed()
        addSubview(label)
        
        label.centerXAnchor.constraintEqualToAnchor(centerXAnchor).active = true
        label.centerYAnchor.constraintEqualToAnchor(centerYAnchor).active = true
        
    }
}
