//
//  CandidateTableViewController.swift
//  HackIllinois
//
//  Created by Sanjeet Suhag on 2/20/16.
//  Copyright © 2016 Sanjeet Suhag. All rights reserved.
//

import UIKit

class CandidateTableViewController: UITableViewController {

    // MARK: Properties
    
    var candidates: Array<Candidate>!
    var selection: Candidate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Store candidates.
        candidates = APIClient.sharedAPIClient.candidates
        selection = nil
        
        self.title = "CANDIDATES"
        self.navigationController?.navigationBar.barStyle = .Default
        
        self.tabBarItem.title = ""
        
        for tabBarItem in (self.tabBarController?.tabBar.items)! {
            tabBarItem.title = "";
            tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.tintColor = UIColor.americanBlue()
        UINavigationBar.appearance().barTintColor = UIColor.americanBlue()

    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        
        self.tabBarController?.tabBar.tintColor = UIColor.americanRed()
        UINavigationBar.appearance().barTintColor = UIColor.americanRed()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.candidates.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("CandidateCell", forIndexPath: indexPath)

        // Configure the cell...
        
        let currentCandidate: Candidate!
        
        currentCandidate = self.candidates[indexPath.row]
    
        // Image View
        
        if let imageView = cell.viewWithTag(2) as? UIImageView {
            imageView.image = currentCandidate.imageData
            imageView.layer.borderWidth = 2
            imageView.layer.borderColor = currentCandidate.party.containsString("Demo") ? UIColor.americanBlue().CGColor : UIColor.americanRed().CGColor
            imageView.layer.masksToBounds = false
            imageView.clipsToBounds = true
            imageView.layer.cornerRadius = 32
        }
        
        // Name
        
        if let nameLabel = cell.viewWithTag(3) as? UILabel {
            nameLabel.text = currentCandidate.name
        }
        
        // Position
        
        if let positionLabel = cell.viewWithTag(4) as? UILabel {
            positionLabel.text = currentCandidate.position
        }
        
        return cell
    }

    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selection = candidates[indexPath.row]
        performSegueWithIdentifier("ShowCandidateSegue", sender: self)
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 128
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "ShowCandidateSegue") {
            
            let pageViewController = segue.destinationViewController as! CandidatePageViewController
            pageViewController.candidate = selection
            
        }
    }

}
