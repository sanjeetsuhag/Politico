//
//  CandidateViewController.swift
//  HackIllinois
//
//  Created by Sanjeet Suhag on 2/20/16.
//  Copyright © 2016 Sanjeet Suhag. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class CandidateViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // MARK: - Properties
    
    var pageIndex: Int?
    var candidate: Candidate!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        tableView.tableFooterView = UIView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.candidate.policies.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        let text = candidate.policies[indexPath.row].information
        let font = UIFont(name: "Lora-Regular", size: 14.0)!
        let width = self.view.frame.width - 10
        
        let height = heightForView(text, font: font, width: width)
        
        if height < 64 {
            return 64
        } else {
            return height
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("CandidateIssueCell", forIndexPath: indexPath)
        cell.textLabel?.text = candidate.policies[indexPath.row].information
        
        cell.textLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.font = UIFont(name: "Lora-Regular", size: 14)
        
        return cell
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label = UILabel(frame: CGRectMake(0, 0, width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        label.text = text
        
        label.layoutIfNeeded()
        label.sizeToFit()
        
        return label.frame.height
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 150
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        
        let backgroundImageView = UIImageView(frame: CGRect(x:0, y: 0, width: self.view.frame.width, height: 150))
        backgroundImageView.contentMode = .ScaleAspectFill
        backgroundImageView.image = candidate.bgImageData
        backgroundImageView.layer.masksToBounds = false
        backgroundImageView.clipsToBounds = true
        
        let tintView = UIView()
        tintView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 150)
        tintView.backgroundColor = UIColor.americanBlue()
        tintView.alpha = 0.75
        backgroundImageView.addSubview(tintView)
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Lora-Bold", size: 16)
        label.text = candidate.name
        label.sizeToFit()
        label.center = backgroundImageView.center
        label.textColor = UIColor.whiteColor()
        backgroundImageView.addSubview(label)
        
        return backgroundImageView
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
