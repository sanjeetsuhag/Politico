//
//  WelcomeViewController.swift
//  HackIllinois
//
//  Created by Sanjeet Suhag on 2/20/16.
//  Copyright © 2016 Sanjeet Suhag. All rights reserved.
//

import Spring
import ChameleonFramework
import FontAwesomeKit
import UIKit

class WelcomeViewController: UIViewController {
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupViews()
        
    }
    
    // MARK: - UI Setup
    
    func setupViews() {
        
        // Scoll view.
        
        let scrollView = UIScrollView()
        scrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        scrollView.scrollEnabled = true
        self.view.addSubview(scrollView)
        
        // Top half.
        
        let topView = UIView()
        topView.backgroundColor = UIColor.flatBlueColor()
        topView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(topView)
        
        topView.center.x = self.view.center.x
        topView.center.y = self.view.center.y - (self.view.frame.height * 0.5)
        
        topView.heightAnchor.constraintEqualToAnchor(self.view.heightAnchor).active = true
        topView.widthAnchor.constraintEqualToAnchor(self.view.widthAnchor).active = true
        
        // Bottom half.
        
        let bottomView = UIView()
        bottomView.backgroundColor = UIColor.flatRedColor()
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(bottomView)
        
        
        bottomView.center.x = self.view.center.x
        bottomView.center.y = self.view.center.y + (self.view.frame.height * 0.5)
        
        bottomView.heightAnchor.constraintEqualToAnchor(self.view.heightAnchor).active = true
        bottomView.widthAnchor.constraintEqualToAnchor(self.view.widthAnchor).active = true
        
        // Top label.
        
        let topLabel = UILabel()
        topLabel.font = UIFont(name: "Lora-Bold", size: 22)
        topLabel.textColor = UIColor.whiteColor()
        topLabel.text = "CANDIDATES"
        topLabel.sizeToFit()
        topLabel.translatesAutoresizingMaskIntoConstraints = false
        topView.addSubview(topLabel)
        
        topLabel.centerXAnchor.constraintEqualToAnchor(topView.centerXAnchor).active = true
        topLabel.centerYAnchor.constraintEqualToAnchor(topView.centerYAnchor, constant: -(topView.frame.height * 0.5)).active = true
        
        // Bottom label.
        
        let bottomLabel = UILabel()
        bottomLabel.font = UIFont(name: "Lora-Bold", size: 22)
        bottomLabel.textColor = UIColor.whiteColor()
        bottomLabel.text = "POLICIES"
        bottomLabel.sizeToFit()
        bottomLabel.translatesAutoresizingMaskIntoConstraints = false
        bottomView.addSubview(bottomLabel)
        
        bottomLabel.centerXAnchor.constraintEqualToAnchor(bottomView.centerXAnchor).active = true
        bottomLabel.centerYAnchor.constraintEqualToAnchor(bottomView.centerYAnchor).active = true
        
        // Swipe Up label.
        
        let swipeUpLabel = UILabel()
        swipeUpLabel.font = UIFont(name: "Lora-Regular", size: 16)
        swipeUpLabel.textColor = UIColor.whiteColor()
        swipeUpLabel.text = "Swipe Up"
        swipeUpLabel.sizeToFit()
        swipeUpLabel.translatesAutoresizingMaskIntoConstraints = false
        bottomView.addSubview(swipeUpLabel)
        
        swipeUpLabel.centerXAnchor.constraintEqualToAnchor(bottomView.centerXAnchor).active = true
        swipeUpLabel.centerYAnchor.constraintEqualToAnchor(bottomView.centerYAnchor, constant: 125).active = true
        
        
        // Swipe Down label.
        
        let swipeDownLabel = UILabel()
        swipeDownLabel.font = UIFont(name: "Lora-Regular", size: 16)
        swipeDownLabel.textColor = UIColor.whiteColor()
        swipeDownLabel.text = "Swipe Down"
        swipeDownLabel.sizeToFit()
        swipeDownLabel.translatesAutoresizingMaskIntoConstraints = false
        topView.addSubview(swipeDownLabel)
        
        swipeDownLabel.centerXAnchor.constraintEqualToAnchor(topView.centerXAnchor).active = true
        swipeDownLabel.centerYAnchor.constraintEqualToAnchor(topView.centerYAnchor, constant: -125).active = true
        
        
        
    }

}
