//
//  CandidatePageViewController.swift
//  HackIllinois
//
//  Created by Sanjeet Suhag on 2/20/16.
//  Copyright © 2016 Sanjeet Suhag. All rights reserved.
//

import Async
import UIKit

class CandidatePageViewController: UIPageViewController {
    
    // MARK: Properties
    
    var pages = [IssueViewController]()
    
    var navigationBar = UINavigationBar()
    
    var candidate: Candidate!
    
    var categories = [Category]()

    var gestureRecognizer: UISwipeGestureRecognizer!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        dataSource = self
        
        self.tabBarItem.title = ""
        
        // Parse through all policies of current candidate.
        gestureRecognizer = UISwipeGestureRecognizer(target: self, action: "handleSwipe:")
        gestureRecognizer.direction = .Down
        
        
        self.setNavigationBarToView()
        
        let page1 = self.storyboard!.instantiateViewControllerWithIdentifier("IssueViewController") as! IssueViewController
        for policy in self.candidate.policies {
            if policy.category == Category.ArtsCulture {
                page1.policies.append(policy)
            }
        }
        
        let page2 = self.storyboard!.instantiateViewControllerWithIdentifier("IssueViewController") as! IssueViewController
        for policy in self.candidate.policies {
            if policy.category == Category.Crime {
                page2.policies.append(policy)
            }
        }
        
        let page3 = self.storyboard!.instantiateViewControllerWithIdentifier("IssueViewController") as! IssueViewController
        for policy in self.candidate.policies {
            if policy.category == Category.Education {
                page3.policies.append(policy)
            }
        }
        
        let page4 = self.storyboard!.instantiateViewControllerWithIdentifier("IssueViewController") as! IssueViewController
        for policy in self.candidate.policies {
            if policy.category == Category.Environment {
                page4.policies.append(policy)
            }
        }
        
        let page5 = self.storyboard!.instantiateViewControllerWithIdentifier("IssueViewController") as! IssueViewController
        for policy in self.candidate.policies {
            if policy.category == Category.Families {
                page5.policies.append(policy)
            }
        }
        
        let page6 = self.storyboard!.instantiateViewControllerWithIdentifier("IssueViewController") as! IssueViewController
        for policy in self.candidate.policies {
            if policy.category == Category.ForeignPolicy {
                page6.policies.append(policy)
            }
        }
        
        let page7 = self.storyboard!.instantiateViewControllerWithIdentifier("IssueViewController") as! IssueViewController
        for policy in self.candidate.policies {
            if policy.category == Category.Government {
                page7.policies.append(policy)
            }
        }
        
        let page8 = self.storyboard!.instantiateViewControllerWithIdentifier("IssueViewController") as! IssueViewController
        for policy in self.candidate.policies {
            if policy.category == Category.Healthcare {
                page8.policies.append(policy)
            }
        }
        
        let page9 = self.storyboard!.instantiateViewControllerWithIdentifier("IssueViewController") as! IssueViewController
        for policy in self.candidate.policies {
            if policy.category == Category.Immigration {
                page9.policies.append(policy)
            }
        }
        
        let page10 = self.storyboard!.instantiateViewControllerWithIdentifier("IssueViewController") as! IssueViewController
        for policy in self.candidate.policies {
            if policy.category == Category.JobsEconomy {
                page10.policies.append(policy)
            }
        }
        
        let page11 = self.storyboard!.instantiateViewControllerWithIdentifier("IssueViewController") as! IssueViewController
        for policy in self.candidate.policies {
            if policy.category == Category.Other {
                page11.policies.append(policy)
            }
        }
        
        let page12 = self.storyboard!.instantiateViewControllerWithIdentifier("IssueViewController") as! IssueViewController
        for policy in self.candidate.policies {
            if policy.category == Category.ScienceTech {
                page12.policies.append(policy)
            }
        }
        
        let page13 = self.storyboard!.instantiateViewControllerWithIdentifier("IssueViewController") as! IssueViewController
        for policy in self.candidate.policies {
            if policy.category == Category.Taxes {
                page13.policies.append(policy)
            }
        }
        
        
        let pagesArray = [page1, page2, page3, page4, page5, page6, page7, page8, page9, page10, page11, page12, page13]
        
        var i = 0
        
        for (_, arr) in pagesArray.enumerate() {
            
            if (arr.policies.count > 0) {
                
                arr.pageIndex = i++
                
                pages.append(arr)
            }
    
        }
        
        
        if let firstPage = pages.first {
            self.setViewControllers([firstPage], direction: .Forward, animated: true, completion: nil)
        } else {
            
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func setNavigationBarToView() {
        
        // Set up navigation bar.
        
        navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 180)
        
        navigationBar.addGestureRecognizer(self.gestureRecognizer)
        
        // Set up background image view.
        
        let backgroundImageView = UIImageView()
        backgroundImageView.frame = CGRect(x: 0, y: 0, width: navigationBar.frame.width, height: navigationBar.frame.height)
        backgroundImageView.contentMode = .ScaleAspectFill
        backgroundImageView.layer.masksToBounds = false
        backgroundImageView.image = candidate.bgImageData
        backgroundImageView.clipsToBounds = true
        
        
        navigationBar.addSubview(backgroundImageView)
        
        let tintView = UIView()
        tintView.frame = backgroundImageView.frame
        tintView.backgroundColor = candidate.party.containsString("Demo") ? UIColor.americanBlue() : UIColor.americanRed()
        tintView.alpha = 0.75
        navigationBar.addSubview(tintView)
        
        let nameLabel = UILabel(frame: CGRect(x: navigationBar.center.x - 30, y: navigationBar.center.y - 30, width: self.view.frame.width - (navigationBar.center.x - 30), height: 30))
        nameLabel.font = UIFont(name: "Lora-Bold", size: 17)
        nameLabel.textColor = UIColor.whiteColor()
        nameLabel.text = candidate.name
        nameLabel.sizeToFit()
        navigationBar.addSubview(nameLabel)
        
        let ageLabel = UILabel(frame: CGRect(x: navigationBar.center.x - 30, y: nameLabel.frame.origin.y + nameLabel.frame.height + 2.5, width: self.view.frame.width - (navigationBar.center.x - 30), height: 20))
        ageLabel.font = UIFont(name: "Lora-Regular", size: 15)
        ageLabel.textColor = UIColor.lightGrayColor()
        ageLabel.text = "\(candidate.age) years old."
        navigationBar.addSubview(ageLabel)
        
        let positionLabel = UILabel(frame: CGRect(x: navigationBar.center.x - 30, y: ageLabel.frame.origin.y + ageLabel.frame.height + 2.5, width: self.view.frame.width - (navigationBar.center.x - 30), height: 40))
        positionLabel.font = UIFont(name: "Lora-Bold", size: 15)
        positionLabel.textColor = UIColor.lightGrayColor()
        positionLabel.text = candidate.position
        positionLabel.lineBreakMode = .ByWordWrapping
        positionLabel.numberOfLines = 2
        navigationBar.addSubview(positionLabel)
        
        let imageViewDimensions: CGFloat = 96
        let imageView = UIImageView(frame: CGRect(x: 30 , y: navigationBar.center.y - (imageViewDimensions / 2), width: imageViewDimensions, height: imageViewDimensions))
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = UIColor.whiteColor().CGColor
        imageView.layer.masksToBounds = false
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = imageView.frame.height / 2
        let image = candidate.imageData
        imageView.image = image
        navigationBar.addSubview(imageView)
        
        
        self.view.addSubview(navigationBar)
    
        
    }
    
    func handleSwipe (gestureRecognizer: UISwipeGestureRecognizer) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.drawInRect(CGRect(origin: CGPointZero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage
    }

}


// MARK: - UIPageViewControllerDataSource

extension CandidatePageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        var index = (viewController as! IssueViewController).pageIndex!
        index--
        
        if (index < 0) {
            index = pages.count - 1
        }
        
        return pages[index]
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        var index = (viewController as! IssueViewController).pageIndex!
        index++
        
        if (index >= pages.count) {
            index = 0
        }
        
        return pages[index]
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
}
