//
//  Colors.swift
//  HackIllinois
//
//  Created by Sanjeet Suhag on 2/20/16.
//  Copyright © 2016 Sanjeet Suhag. All rights reserved.
//

import Hue
import Foundation


extension UIColor {

    class func americanBlue() -> UIColor {
        return UIColor.hex("#0052A5")
    }
    
    class func americanRed() -> UIColor {
        return UIColor.hex("#E0162B")
    }
}